# StatsYard

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `stats_yard` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:stats_yard, "~> 0.1.0"}]
    end
    ```

  2. Ensure `stats_yard` is started before your application:

    ```elixir
    def application do
      [applications: [:stats_yard]]
    end
    ```

