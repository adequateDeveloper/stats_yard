defmodule StatsYard do
  use Application
  import Supervisor.Spec, warn: false


  def start(_type, _args) do
    start_stat_shipper()
    start_timestamp_writer()
    start_ingest_consumer()
  end

  def start_stat_shipper() do
    import Supervisor.Spec, warn: false

    # Connecting Statix port here instead of in StatsYard.StatixShipper since a
    # dead StatsYard GenServer + a running Statix port makes erlang attempt to
    # register the same port twice and throw exceptions
    :ok = StatsYard.StatShipper.connect()

    children = [
      worker(StatsYard.StatShipper, [])
    ]

    opts = [strategy: :one_for_one, name: StatsYard.StatShipperSupervisor]
    Supervisor.start_link(children, opts)
  end

  def start_timestamp_writer do
    children = [
      worker(__MODULE__.TimestampWriter, [])
    ]

    opts = [strategy: :one_for_one, name: __MODULE__.TimestampWriterSupervisor]
    Supervisor.start_link(children, opts)
  end

  def start_ingest_consumer() do
    # note: starts in the order listed
    children = [
      worker(BlockingQueue, [1000, [name: ingest_queue]]),
      worker(__MODULE__.IngestConsumer, [ingest_queue, [name: ingest_consumer]])
    ]

    # If a child process terminates, the 'rest' of associated child processes *after* the terminated one in start
    # order, are terminated. Then the terminated child process and the rest of the child processes are restarted.
    # This will prevent the ingest_consumer from holding onto a Stream function that references a dead ingest_queue
    # process.
    opts = [strategy: :rest_for_one, name: __MODULE__.IngestSupervisor]
    Supervisor.start_link(children, opts)
  end

  # Convenience functions to return names of GenServer processes
  def ingest_queue, do: :ingest_queue
  def ingest_consumer, do: :ingest_consumer

end
