defmodule StatsYard.DataPoint do
  require Logger

  @moduledoc """
  A custom data type by which data points can be continuously queued, consumed and validated.
  """

  @typedoc """
  A struct of type {metric: String.t, value: number, entity: String.t}
  """
  @type t :: %__MODULE__{metric: String.t, value: number, entity: String.t}
  defstruct [:metric, :value, :entity]

  @doc """
  Validate the given `datapoint`.

  ## Examples
      iex> StatsYard.DataPoint.validate(%StatsYard.DataPoint{metric: "metric", value: 1, entity: "entity"})
      {:ok, %StatsYard.DataPoint{metric: "metric", value: 1, entity: "entity"}}

      iex> StatsYard.DataPoint.validate(%StatsYard.DataPoint{metric: "metric", value: "1", entity: "entity"})
      {:invalid, %StatsYard.DataPoint{metric: "metric", value: "1", entity: "entity"}}
  """
  def validate(%__MODULE__{metric: metric, value: value, entity: entity})
    when is_binary(metric)
     and is_number(value)
     and is_binary(entity) do

    Logger.debug "valid datapoint: #{inspect(%__MODULE__{metric: metric, value: value, entity: entity})}"
    {:ok, %__MODULE__{metric: metric, value: value, entity: entity}}
  end

  def validate(value) do
    Logger.warn "invalid datapoint: #{inspect(value)}"
    {:invalid, value}
  end

end