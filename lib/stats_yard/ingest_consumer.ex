defmodule StatsYard.IngestConsumer do
  use GenServer
  require Logger


  ## API

  def start_link(ingest_queue_pid, opts \\ []) do
    GenServer.start_link(__MODULE__, ingest_queue_pid, opts)
  end


  ## Callbacks

  # start the queue stream loop process and save its pid as state
  def init(ingest_queue_pid) do
    create_metrics
    spawn_link fn -> consume_data_points(ingest_queue_pid) end
    {:ok, ingest_queue_pid}
  end


  ## Privates

  # define metrics so that Folsom can get the appropriate structures set up in memory
  defp create_metrics do
    :ok = :folsom_metrics.new_histogram "validation.time.us"
    :ok = :folsom_metrics.new_meter "validation.rate"
  end

  # Monitor the queue stream and validate each item arriving.
  # if valid DataPoint then return the DataPoint
  # else return nil - note: a warn message is logged by validate/1
  #
  # Wrap validate/1 with Folsom's histogram_timed_update/2.
  # This Folsom function is basically a pass-through function, in that it accepts a function of our choosing as one of
  # its arguments, evaluates that function and does some kind of work in relation to it, and then returns the value of
  # the function that it evaluated. In this case, the work done in addition to our validation function is timing its run
  # time and updating an ETS table that Folsom maintains for "doing math" on and maintaining a history for our metric.
  # From our perspective, though, almost nothing has changed. We don't get any new return values or anything from
  # validate/1,  and so we can still use it in the same manner we wanted to originally.
  defp consume_data_points(ingest_queue_pid) do
    for item <- BlockingQueue.pop_stream(ingest_queue_pid) do
      case :folsom_metrics.histogram_timed_update("ingest.validation.time.us",
        fn -> StatsYard.DataPoint.validate(item) end, []) do
        {:ok, datapoint} -> datapoint
        {:invalid, item} -> item
      end

      # After we've validated the data point and gotten a timing for the invocation, we increment the counter
      # validate.rate, and then move on.
      :folsom_metrics.notify{"validation.rate", 1}
    end
  end

end