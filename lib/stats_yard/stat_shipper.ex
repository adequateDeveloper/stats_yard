defmodule StatsYard.StatShipper do
  @moduledoc """
  GenServer for reporting metrics to statsd on a configurable-interval
  """

  use GenServer
  use Statix
  require Logger

  @shippable_histogram_stats [:arithmetic_mean, :harmonic_mean, :percentile, :standard_deviation]
  @shippable_meter_stats [:one, :five, :fifteen]


  ## API

  def start_link(ship_interval \\ 1000) do
    GenServer.start_link(__MODULE__, ship_interval)
  end


  # Callbacks

  def init(ship_interval) do
    schedule_shipment(ship_interval)
    {:ok, []}
  end

  def handle_info({:ship, ship_interval}, state) do
    state = ship(state)
    schedule_shipment(ship_interval)
    {:noreply, state}
  end


  ## Privates

  defp schedule_shipment(ship_interval) do
    Process.send_after self, {:ship, ship_interval}, ship_interval
  end

  defp ship(state) do
    for metric_tuple <- :folsom_metrics.get_metrics_info do
      {metric, [type: type, tags: _tags]} = metric_tuple
      case type do
        :histogram    -> ship_histogram_stats metric
        :meter_reader -> ship_meter_stats metric
        :meter        -> ship_meter_stats metric
        :counter      -> __MODULE__.counter(:folsom_metrics.get_metric_value(metric))
        :gauge        -> __MODULE__.gauge(:folsom_metrics.get_metric_value(metric))
        :history      -> nil
      end

      Logger.debug "#{inspect(__MODULE__)} :: Found metric #{inspect(metric)} -> #{inspect(type)}"
    end

    state
  end

  defp ship_meter_stats(metric) do
    stats = :folsom_metrics.get_metric_value metric

    for key <- @shippable_meter_stats do
      __MODULE__.gauge "#{metric}.#{Atom.to_string(key)}", stats[key]
    end
  end

  defp ship_histogram_stats(metric) do
    stats = :folsom_metrics.get_histogram_statistics metric

    for key <- @shippable_histogram_stats do
      case key do
        :percentile ->
          [{50, p50}, {75, p75}, {95, p95}, {99, p99}, {999, p999}] = stats[key]
          __MODULE__.gauge "#{metric}.p50", p50
          __MODULE__.gauge "#{metric}.p75", p75
          __MODULE__.gauge "#{metric}.p95", p95
          __MODULE__.gauge "#{metric}.p99", p99
          __MODULE__.gauge "#{metric}.p999", p999
        shippable ->
          __MODULE__.gauge "#{metric}.#{shippable}", stats[shippable]
      end
    end
  end

end