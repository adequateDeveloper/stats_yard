defmodule StatsYard.Mixfile do
  use Mix.Project

  def project do
    [app: :stats_yard,
     version: "0.1.0",
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:statix, :ex_vmstats, :folsom, :logger],
     mod: {StatsYard, []}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:blocking_queue, "~> 1.3"},                  # A simple queue implemented as a GenServer
      {:credo, "~> 0.3", only: [:dev, :test]},      # A static code analysis tool
      {:folsom, "~> 0.8.3"},                          # Erlang based metrics system
      {:statix, "~> 0.7"},                          # An Elixir client for StatD compatible servers
      {:ex_vmstats, "~> 0.0.1"}                     # A package for pushing Erlang VM stats into StatsD
    ]
  end
end
