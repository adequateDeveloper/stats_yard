defmodule StatsYard.DataPointTest do
  use ExUnit.Case, async: true
  import ExUnit.CaptureLog
  alias StatsYard.DataPoint

  doctest DataPoint

  @valid_data_points [
    %DataPoint{metric: "testmetric", value: 123, entity: "testentity"}
  ]

  @invalid_data_points [
    %DataPoint{metric: :testmetric,   value: 123,    entity: "testentity"},
    %DataPoint{metric: "testmetric",  value: "123",  entity: "testentity"},
    %DataPoint{metric: "testmetric",  value: 123,    entity: :testentity}
  ]


  test "can validate valid data points" do
    for valid_data_point <- @valid_data_points do
      assert capture_log([level: :debug], fn ->
        assert {:ok, ^valid_data_point} = DataPoint.validate(valid_data_point)
      end) =~ "valid datapoint: #{inspect(valid_data_point)}"
    end
  end

  test "can validate invalid data points" do
    for invalid_data_point <- @invalid_data_points do
      assert capture_log([level: :warn], fn ->
        assert {:invalid, ^invalid_data_point} = DataPoint.validate(invalid_data_point)
      end) =~ "invalid datapoint: #{inspect(invalid_data_point)}"
    end
  end

end