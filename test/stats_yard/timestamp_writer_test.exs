defmodule StatsYard.TimestampWriterTest do
  use ExUnit.Case, async: false                 # don't run tests in parallel

  alias StatsYard.TimestampWriter

  doctest TimestampWriter

  setup context do
    if cd = context[:cd] do                     # is the current test utilizing the :cd tag?
      prev_cd = File.cwd!                       #   save the cwd
      File.cd!(cd)                              #   switch to the directory requested by the current test
      on_exit fn ->                             #   on test completion (regardless of success or failure):
        if tempfile = context[:tempfile] do     #     is the current test utilizing the :tempfile tag?
          if File.exists?(tempfile) do          #       does the tempfile exist?
            File.rm!(tempfile)                  #         remove the tempfile
          end                                   #
        end                                     #
        File.cd!(prev_cd)                       #     switch back to the previous cwd
      end
    end
    :ok
  end

  @tag cd: "test/fixtures"                      # cwd for this test - must previously exist
  @tag tempfile: "tstamp_write.test"
  test "can write timestamp to a file", context do
    assert :ok == TimestampWriter.write_timestamp(context[:tempfile], :os.system_time(1000))
  end

end